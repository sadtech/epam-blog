package ru.epam.blog.core.entity.enums;

public enum StatusPost {

    DRAFT, PUBLISHED, HIDDEN

}
