package ru.epam.blog.core.service;

import ru.epam.blog.core.entity.Person;

public interface AuthService {

    Person getPersonAuth();

}
